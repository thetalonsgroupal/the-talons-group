The Talons Group is a boutique real estate and development firm serving Auburn and Opelika, Alabama. The group was established to introduce an elevated real estate experience to the area based on cutting-edge services and strategy, as well as a higher level of customer service.

Address: 220 N College St, Auburn, AL 36830, USA

Phone: 334-758-6220

Website: http://thetalonsgroup.com
